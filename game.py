from random import randint

player_name = input("Hi, What's your name? Can I guess your birthday?  ")


for guess_number in range(1,6):
    month_number = randint(1, 12)
    year_number = randint (1922, 2022)

    print("Guess", guess_number, "Were you born in", month_number, "/", year_number, "?")

    response = input ("yes or no?")

    if response == "yes":
        print("Aha! I knew that's when it was!")
        exit()
    elif guess_number == 5:
        print("I have better things to do with my time! Goodbye!")
    else:
        print("Well hold on now, let me try again!")
